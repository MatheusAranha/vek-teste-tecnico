import { Component, OnInit } from '@angular/core';
import { ProposalHandlerService } from '../proposal-handler.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-proposal',
  templateUrl: './new-proposal.component.html',
  styleUrls: ['./new-proposal.component.css']
})
export class NewProposalComponent implements OnInit {
  proposalForm;
  errorMessage = '';
  successMessage = '';

  activities;
  selectedActivity;

  rivals;
  selectedRival;

  constructor(
    private proposalHandlerService: ProposalHandlerService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) { 
    this.proposalForm = this.formBuilder.group({
      status: [''],
      user: [''],
      cnpjOrCpf: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: [''],
      rivalName: ['', Validators.required],
      rivalDebitTax: [0, Validators.required],
      ribalCreditTax: [0, Validators.required],
      activityName: ['', Validators.required],
      proposalDebitTax: [0, Validators.required],
      proposalCreditTax: [0, Validators.required]
    });
    this.proposalHandlerService.activitiesChange.subscribe(value => {
      this.activities = proposalHandlerService.activities;
    });
    this.proposalHandlerService.rivalsChange.subscribe(value => {
      this.rivals = proposalHandlerService.rivals;
    });
    this.proposalHandlerService.successMessageChange.subscribe(value => {
      this.successMessage = proposalHandlerService.successMessage;
      setTimeout(() => {
        this.successMessage = "";
      }, 5000);
    });
  }

  ngOnInit(): void {
    this.proposalHandlerService.getActivities();
    this.proposalHandlerService.getRivals();
  }

  public changeRival() {
    for(const e in this.rivals) {
      if(this.rivals[e].name === this.proposalForm.value.rivalName) {
        this.selectedRival = this.rivals[e];
        this.proposalForm.patchValue({rivalDebitTax: this.rivals[e].debitTax});
        this.proposalForm.patchValue({ribalCreditTax: this.rivals[e].creditTax});
      }
    }
  }

  public changeActivity() {
    for(const e in this.activities) {
      if(this.activities[e].name === this.proposalForm.value.activityName) {
        this.selectedActivity = this.activities[e];
        this.proposalForm.patchValue({proposalDebitTax: this.activities[e].debitMin});
        this.proposalForm.patchValue({proposalCreditTax: this.activities[e].creditMin});
      }
    }
  }

  public submitProposal(selectedStatus) {
    this.proposalForm.patchValue({user: sessionStorage.getItem("user")});
    this.proposalForm.patchValue({status: selectedStatus});
    this.proposalHandlerService.handleNewProposal(this.proposalForm.value);
    this.modalService.dismissAll();
    this.proposalForm = this.formBuilder.group({
      status: [''],
      user: [''],
      cnpjOrCpf: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: [''],
      rivalName: ['', Validators.required],
      rivalDebitTax: [0, Validators.required],
      ribalCreditTax: [0, Validators.required],
      activityName: ['', Validators.required],
      proposalDebitTax: [0, Validators.required],
      proposalCreditTax: [0, Validators.required]
    });
  }

  public onSubmit(data, content) {
    if(this.proposalForm.valid) {
      if(data.proposalDebitTax < this.selectedActivity.debitMin) {
        this.changeErrorMessage("A taxa de débito da proposta está inferior à taxa permitida para esta atividade. A taxa mínima permitida é " + this.selectedActivity.debitMin + "%.");
        return;
      } else if (data.proposalCreditTax < this.selectedActivity.creditMin) {
        this.changeErrorMessage("A taxa de crédito da proposta está inferior à taxa permitida para esta atividade. A taxa mínima permitida é " + this.selectedActivity.creditMin + "%.");
        return;
      } else {
        this.modalService.open(content, { size: 'lg' });
      }
    } else {
      for(const e in this.proposalForm.controls) {
        if(!this.proposalForm.controls[e].valid) {
          switch (e) {
            case "cnpjOrCpf":
                this.changeErrorMessage("O campo CNPJ ou CPF é obrigatório.");
                return;
              break;

            case "phoneNumber":
                this.changeErrorMessage("O campo Telefone é obrigatório.");
                return;
              break;
          
            case "rivalName":
                this.changeErrorMessage("O campo Concorrente é obrigatório.");
                return;
              break;

            case "rivalDebitTax":
                this.changeErrorMessage("O campo Taxa de débito do concorrente é obrigatório.");
                return;
              break;
              
            case "ribalCreditTax":
                this.changeErrorMessage("O campo Taxa de crédito do concorrente é obrigatório.");
                return;
              break;
            
            case "activityName":
                this.changeErrorMessage("O campo Ramo de atividade é obrigatório.");
                return;
              break;

            case "proposalDebitTax":
                this.changeErrorMessage("O campo Taxa de débito da proposta é obrigatório.");
                return;
              break;

            case "proposalCreditTax":
                this.changeErrorMessage("O campo Taxa de crédito da proposta é obrigatório.");
                return;
              break;
            default:
              break;
          }
        }
      }
      this.changeErrorMessage("Verifique os campos obrigatórios.");
      return;
    }
  }

  public changeErrorMessage(message: string) {
    this.errorMessage = message;
    setTimeout(() => {
      this.errorMessage = "";
    }, 5000);
  }

  public closeAlert() {
    this.errorMessage = "";
  }

  public closeSuccessAlert() {
    this.errorMessage = "";
  }

}
