import { Component, OnInit } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { LoginHandlerService } from '../login-handler.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  checkoutForm;
  errorMessage = '';

  constructor(
    private loginHandlerService: LoginHandlerService,
    private formBuilder: FormBuilder,
  ) {
      this.checkoutForm = this.formBuilder.group({
        name: ['', Validators.required],
        password: ['', Validators.required]
      });
      this.loginHandlerService.loginErrorMessageChange.subscribe(value => {
        this.errorMessage = loginHandlerService.loginErrorMessage;
        setTimeout(() => {
          this.errorMessage = "";
        }, 5000);
      })
   }

  ngOnInit(): void {
  }

  public onSubmit(data) {
    if(this.checkoutForm.valid) {
      this.loginHandlerService.handleLogin(data);
    } else {
      for(const e in this.checkoutForm.controls) {
        if(!this.checkoutForm.controls[e].valid) {
          switch (e) {
            case "name":
                this.changeErrorMessage("O campo Usuário é obrigatório.");
                return;
              break;

            case "password":
                this.changeErrorMessage("O campo Senha é obrigatório.");
                return;
              break;
          
            default:
              break;
          }
        }
      }
      this.changeErrorMessage("Verifique os campos obrigatórios.");
      return;
    }
  }

  public changeErrorMessage(message: string) {
    this.errorMessage = message;
    setTimeout(() => {
      this.errorMessage = "";
    }, 5000);
  }

  public closeAlert() {
    this.errorMessage = "";
  }

}
