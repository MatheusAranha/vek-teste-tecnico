import { TestBed } from '@angular/core/testing';

import { ProposalHandlerService } from './proposal-handler.service';

describe('ProposalHandlerService', () => {
  let service: ProposalHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProposalHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
