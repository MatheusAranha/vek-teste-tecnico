import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProposalHandlerService {
  successMessage: string;
  successMessageChange: Subject<string> = new Subject<string>();

  proposals: Object;
  proposalsChange: Subject<Object> = new Subject<Object>();

  activities: Object;
  activitiesChange: Subject<Object> = new Subject<Object>();

  rivals: Object;
  rivalsChange: Subject<Object> = new Subject<Object>();

  constructor(private http: HttpClient) {
    this.proposalsChange.subscribe(value => {
      this.proposals = value;
    });
    this.activitiesChange.subscribe(value => {
      this.activities = value;
    })
    this.rivalsChange.subscribe(value => {
      this.rivals = value;
    })
    this.successMessageChange.subscribe(value => {
      this.successMessage = value;
    })
   }

   public getProposals() {
     this.http.get('http://localhost:8080/proposals').subscribe({
       next: data => {
         this.proposalsChange.next(data);
       }
     });
   }

   public getActivities() {
    this.http.get('http://localhost:8080/activities').subscribe({
      next: data => {
        this.activitiesChange.next(data);
      }
    });
  }

  public getRivals() {
    this.http.get('http://localhost:8080/rivals').subscribe({
      next: data => {
        this.rivalsChange.next(data);
      }
    });
  }

  public handleNewProposal(proposalData) {
    this.http.post<any>('http://localhost:8080/proposals/newProposal', proposalData).subscribe({
      next: data => {
        this.successMessageChange.next("Proposta registrada com sucesso.");
      }
    })
  }
}
