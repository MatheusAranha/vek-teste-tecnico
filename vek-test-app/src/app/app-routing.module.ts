import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NewProposalComponent } from './new-proposal/new-proposal.component';
import { ProposalsComponent } from './proposals/proposals.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'new-proposal', component: NewProposalComponent },
  { path: 'proposals', component: ProposalsComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }