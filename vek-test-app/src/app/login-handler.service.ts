import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginHandlerService {
  loginErrorMessage: string = '';
  loginErrorMessageChange: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient, private router: Router) {
    this.loginErrorMessageChange.subscribe((value) => {
      this.loginErrorMessage = value;
    })
   }

  public handleLogin(loginData) {
    this.http.post<any>('http://localhost:8080/users/login', loginData).subscribe({
      next: data => {
        sessionStorage.clear();
        sessionStorage.setItem("id", data.id);
        sessionStorage.setItem("role", data.role);
        sessionStorage.setItem("user", data.name);
        this.router.navigateByUrl("/proposals");
      },
      error: error => {
        switch (error.status) {
          case 403:
            this.loginErrorMessageChange.next("Senha inválida.");
            break;
          case 404:
            this.loginErrorMessageChange.next("Usuário não encontrado.");
            break;
        
          default:
            this.loginErrorMessageChange.next("Aconteceu um erro, por favor tente novamente.");
            break;
        }
      }
    })
  }
}
