import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  role;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.role = sessionStorage.getItem("role");
  }

  public logout() {
    sessionStorage.clear();
    this.router.navigateByUrl("/login");
  }

}
