import { Component, OnInit } from '@angular/core';
import { ProposalHandlerService } from '../proposal-handler.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-proposals',
  templateUrl: './proposals.component.html',
  styleUrls: ['./proposals.component.css']
})
export class ProposalsComponent implements OnInit {

  proposals;

  constructor(private proposalHandlerService: ProposalHandlerService) {
    this.proposalHandlerService.proposalsChange.subscribe(value => {
      this.proposals = this.proposalHandlerService.proposals;
    })
   }

  ngOnInit(): void {
    this.proposalHandlerService.getProposals();
  }

  public addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  public tsToDate(time) {
    const thisDate = new Date(time);
    const day = this.addZero(thisDate.getDate());
    const month = this.addZero(thisDate.getMonth());
    const year = this.addZero(thisDate.getFullYear());
    const hour = this.addZero(thisDate.getHours());
    const minutes = this.addZero(thisDate.getMinutes());
    const seconds = this.addZero(thisDate.getSeconds());
    return day + '/' + month + '/' + year + ' ' + hour + ':' + minutes + ':' + seconds;
  }

  public exportAsCsv(data: any) {
    let csvData = [];
    for(const e in data) {
      const tempProp = {
        "ID": data[e].id,
        "Usuário": data[e].user,
        "Data": this.tsToDate(data[e].timeStamp),
        "Taxa de débito proposta": data[e].proposalDebitTax + "%",
        "Taxa de crédito proposta": data[e].proposalCreditTax + "%",
        "Situação": data[e].status,
        "Concorrente": data[e].rivalName,
        "Taxa de débito do concorrente": data[e].rivalDebitTax + "%",
        "Taxa de crédito do concorrente": data[e].ribalCreditTax + "%",
        "Email": data[e].email,
        "CNPJ/CPF": data[e].cnpjOrCpf,
        "Telefone": data[e].phoneNumber,
        "Ramo": data[e].activityName
      }
      csvData.push(tempProp);
    }
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(csvData[0]);
    let csv = csvData.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    let csvArray = csv.join('\r\n');

    var blob = new Blob([csvArray], {type: 'text/csv' })
    saveAs(blob, "Propostas.csv");
  }

}
