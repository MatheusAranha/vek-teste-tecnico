package com.vektestbackend.server.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vektestbackend.server.domain.Activity;

@Repository
public interface ActivityRepository extends MongoRepository<Activity, String> {

}
