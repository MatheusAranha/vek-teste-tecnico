package com.vektestbackend.server.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.vektestbackend.server.domain.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

	@Query("{'name' : ?0}")
	Optional<User> findUserByName(String name);
}
