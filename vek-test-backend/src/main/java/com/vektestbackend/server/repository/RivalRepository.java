package com.vektestbackend.server.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vektestbackend.server.domain.Rival;

@Repository
public interface RivalRepository extends MongoRepository<Rival, String>{
	
}
