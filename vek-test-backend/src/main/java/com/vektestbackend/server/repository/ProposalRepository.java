package com.vektestbackend.server.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.vektestbackend.server.domain.Proposal;

@Repository
public interface ProposalRepository extends MongoRepository<Proposal, String> {

}
