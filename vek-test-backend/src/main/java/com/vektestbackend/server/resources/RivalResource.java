package com.vektestbackend.server.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vektestbackend.server.domain.Rival;
import com.vektestbackend.server.dto.RivalDTO;
import com.vektestbackend.server.services.RivalService;

@RestController
@RequestMapping(value="/rivals")
public class RivalResource {
	
	@Autowired
	private RivalService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<RivalDTO>> findAll() {
		List<Rival> list = service.findAll();
		List<RivalDTO> listDto = list.stream().map(x -> new RivalDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<RivalDTO> findById(@PathVariable String id) {
		Rival obj = service.findById(id);
		return ResponseEntity.ok().body(new RivalDTO(obj));
	}
}
