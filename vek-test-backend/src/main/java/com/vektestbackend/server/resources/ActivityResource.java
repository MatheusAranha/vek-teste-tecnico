package com.vektestbackend.server.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vektestbackend.server.domain.Activity;
import com.vektestbackend.server.dto.ActivityDTO;
import com.vektestbackend.server.services.ActivityService;

@RestController
@RequestMapping(value="/activities")
public class ActivityResource {

	@Autowired
	private ActivityService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ActivityDTO>> findAll() {
		List<Activity> list = service.findAll();
		List<ActivityDTO> listDto = list.stream().map(x -> new ActivityDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ActivityDTO> findById(@PathVariable String id) {
		Activity obj = service.findById(id);
		return ResponseEntity.ok().body(new ActivityDTO(obj));
	}
}
