package com.vektestbackend.server.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vektestbackend.server.domain.User;
import com.vektestbackend.server.dto.LoginDTO;
import com.vektestbackend.server.dto.UserDTO;
import com.vektestbackend.server.services.UserService;

@RestController
@RequestMapping(value="/users")
public class UserResource {

	@Autowired
	private UserService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<UserDTO>> findAll() {
		List<User> list = service.findAll();
		List<UserDTO> listDto = list.stream().map(x -> new UserDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<UserDTO> findById(@PathVariable String id) {
		User obj = service.findById(id);
		return ResponseEntity.ok().body(new UserDTO(obj));
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ResponseEntity<UserDTO> login(@RequestBody LoginDTO loginDto) {
		User obj = service.findByName(loginDto.getName());
		if(obj.getPassword().equals(loginDto.getPassword())) {
			return ResponseEntity.ok().body(new UserDTO(obj));
		}
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new UserDTO());
		
	}
}
