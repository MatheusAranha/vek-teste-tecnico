package com.vektestbackend.server.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vektestbackend.server.domain.Proposal;
import com.vektestbackend.server.dto.ProposalDTO;
import com.vektestbackend.server.services.ProposalService;

@RestController
@RequestMapping(value="/proposals")
public class ProposalResource {

	@Autowired
	private ProposalService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ProposalDTO>> findAll() {
		List<Proposal> list = service.findAll();
		List<ProposalDTO> listDto = list.stream().map(x -> new ProposalDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/newProposal", method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody ProposalDTO proposalDto) {
		Proposal obj = service.fromDTO(proposalDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
}
