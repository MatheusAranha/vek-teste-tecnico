package com.vektestbackend.server.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vektestbackend.server.domain.Rival;
import com.vektestbackend.server.repository.RivalRepository;
import com.vektestbackend.server.services.exceptions.ObjectNotFoundException;

@Service
public class RivalService {

	@Autowired
	private RivalRepository repo;
	
	public List<Rival> findAll() {
		return repo.findAll();
	}
	
	public Rival findById(String id) {
		Optional<Rival> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}
}
