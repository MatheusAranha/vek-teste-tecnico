package com.vektestbackend.server.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vektestbackend.server.domain.Activity;
import com.vektestbackend.server.repository.ActivityRepository;
import com.vektestbackend.server.services.exceptions.ObjectNotFoundException;

@Service
public class ActivityService {

	@Autowired
	private ActivityRepository repo;
	
	public List<Activity> findAll() {
		return repo.findAll();
	}
	
	public Activity findById(String id) {
		Optional<Activity> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}
}
