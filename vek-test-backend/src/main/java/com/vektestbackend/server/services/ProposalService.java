package com.vektestbackend.server.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vektestbackend.server.domain.Proposal;
import com.vektestbackend.server.dto.ProposalDTO;
import com.vektestbackend.server.repository.ProposalRepository;
import com.vektestbackend.server.services.exceptions.ObjectNotFoundException;

@Service
public class ProposalService {
	
	@Autowired
	private ProposalRepository repo;
	
	public List<Proposal> findAll() {
		return repo.findAll();
	}
	
	public Proposal insert(Proposal obj) {
		return repo.insert(obj);
	}
	
	public Proposal fromDTO(ProposalDTO objDto) {
		return new Proposal(objDto.getId(), objDto.getStatus(), objDto.getUser(), System.currentTimeMillis(), objDto.getRivalName(), objDto.getCnpjOrCpf(), objDto.getPhoneNumber(), objDto.getEmail(), objDto.getActivityName(), objDto.getRivalDebitTax(), objDto.getProposalDebitTax(), objDto.getRibalCreditTax(), objDto.getProposalCreditTax());
	}
}
