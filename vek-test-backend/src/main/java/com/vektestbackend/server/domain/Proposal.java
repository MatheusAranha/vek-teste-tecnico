package com.vektestbackend.server.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Proposal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String status;
	private String user;
	private long timeStamp;
	private String rivalName;
	private String cnpjOrCpf;
	private String phoneNumber;
	private String email;
	private String activityName;
	private BigDecimal rivalDebitTax;
	private BigDecimal proposalDebitTax;
	private BigDecimal ribalCreditTax;
	private BigDecimal proposalCreditTax;
	
	public Proposal() {
		
	}

	public Proposal(String id, String status, String user, long timeStamp, String rivalName, String cnpjOrCpf,
			String phoneNumber, String email, String activityName, BigDecimal rivalDebitTax,
			BigDecimal proposalDebitTax, BigDecimal ribalCreditTax, BigDecimal proposalCreditTax) {
		super();
		this.id = id;
		this.status = status;
		this.user = user;
		this.timeStamp = timeStamp;
		this.rivalName = rivalName;
		this.cnpjOrCpf = cnpjOrCpf;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.activityName = activityName;
		this.rivalDebitTax = rivalDebitTax;
		this.proposalDebitTax = proposalDebitTax;
		this.ribalCreditTax = ribalCreditTax;
		this.proposalCreditTax = proposalCreditTax;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getRivalName() {
		return rivalName;
	}

	public void setRivalName(String rivalName) {
		this.rivalName = rivalName;
	}

	public String getCnpjOrCpf() {
		return cnpjOrCpf;
	}

	public void setCnpjOrCpf(String cnpjOrCpf) {
		this.cnpjOrCpf = cnpjOrCpf;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public BigDecimal getRivalDebitTax() {
		return rivalDebitTax;
	}

	public void setRivalDebitTax(BigDecimal rivalDebitTax) {
		this.rivalDebitTax = rivalDebitTax;
	}

	public BigDecimal getProposalDebitTax() {
		return proposalDebitTax;
	}

	public void setProposalDebitTax(BigDecimal proposalDebitTax) {
		this.proposalDebitTax = proposalDebitTax;
	}

	public BigDecimal getRibalCreditTax() {
		return ribalCreditTax;
	}

	public void setRibalCreditTax(BigDecimal ribalCreditTax) {
		this.ribalCreditTax = ribalCreditTax;
	}

	public BigDecimal getProposalCreditTax() {
		return proposalCreditTax;
	}

	public void setProposalCreditTax(BigDecimal proposalCreditTax) {
		this.proposalCreditTax = proposalCreditTax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proposal other = (Proposal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
