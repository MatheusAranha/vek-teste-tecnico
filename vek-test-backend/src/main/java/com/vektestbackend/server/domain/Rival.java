package com.vektestbackend.server.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Rival implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String name;
	private BigDecimal debitTax;
	private BigDecimal creditTax;
	
	public Rival() {
		
	}

	public Rival(String id, String name, BigDecimal debitTax, BigDecimal creditTax) {
		super();
		this.id = id;
		this.name = name;
		this.debitTax = debitTax;
		this.creditTax = creditTax;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDebitTax() {
		return debitTax;
	}

	public void setDebitTax(BigDecimal debitTax) {
		this.debitTax = debitTax;
	}

	public BigDecimal getCreditTax() {
		return creditTax;
	}

	public void setCreditTax(BigDecimal creditTax) {
		this.creditTax = creditTax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rival other = (Rival) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
