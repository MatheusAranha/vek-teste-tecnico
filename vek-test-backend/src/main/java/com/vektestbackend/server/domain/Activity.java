package com.vektestbackend.server.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Activity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String name;
	private BigDecimal debitMin;
	private BigDecimal creditMin;
	
	public Activity() {
		
	}

	public Activity(String id, String name, BigDecimal debitMin, BigDecimal creditMin) {
		super();
		this.id = id;
		this.name = name;
		this.debitMin = debitMin;
		this.creditMin = creditMin;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDebitMin() {
		return debitMin;
	}

	public void setDebitMin(BigDecimal debitMin) {
		this.debitMin = debitMin;
	}

	public BigDecimal getCreditMin() {
		return creditMin;
	}

	public void setCreditMin(BigDecimal creditMin) {
		this.creditMin = creditMin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Activity other = (Activity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
