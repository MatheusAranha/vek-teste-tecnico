package com.vektestbackend.server.config;

import java.math.BigDecimal;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import com.vektestbackend.server.domain.Activity;
import com.vektestbackend.server.domain.Proposal;
import com.vektestbackend.server.domain.Rival;
import com.vektestbackend.server.domain.User;
import com.vektestbackend.server.repository.ActivityRepository;
import com.vektestbackend.server.repository.ProposalRepository;
import com.vektestbackend.server.repository.RivalRepository;
import com.vektestbackend.server.repository.UserRepository;

@Configuration
public class Instantiation implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RivalRepository rivalRepository;
	
	@Autowired
	private ActivityRepository activityRepository;
	
	@Autowired
	private ProposalRepository proposalRepository;
	
	@Override
	public void run(String... args) throws Exception {
		
		userRepository.deleteAll();
		rivalRepository.deleteAll();
		activityRepository.deleteAll();
		proposalRepository.deleteAll();
		
		User user1 = new User(null, "user1", "123a", "operator");
		User user2 = new User(null, "user2", "123b", "operator");
		User admin = new User(null, "admin", "abc1", "admin");
		
		Rival rival1 = new Rival(null, "Concorrente A", new BigDecimal("2.00"), new BigDecimal("2.50"));
		Rival rival2 = new Rival(null, "Concorrente B", new BigDecimal("4.00"), new BigDecimal("1.50"));
		Rival rival3 = new Rival(null, "Concorrente C", new BigDecimal("3.00"), new BigDecimal("3.00"));
		
		Activity activity1 = new Activity(null, "Atividade 1", new BigDecimal("1.00"), new BigDecimal("1.50"));
		Activity activity2 = new Activity(null, "Atividade 2", new BigDecimal("4.00"), new BigDecimal("0.50"));
		Activity activity3 = new Activity(null, "Atividade 3", new BigDecimal("2.00"), new BigDecimal("2.50"));
		Activity activity4 = new Activity(null, "Atividade 4", new BigDecimal("8.00"), new BigDecimal("8.50"));
		
		Proposal proposal1 = new Proposal(null, "Aprovada", "user1", System.currentTimeMillis(), "Concorrente A", "000.000.000-00", "12345678", "green@gmail.com", "Atividade 1", new BigDecimal("2.00"), new BigDecimal("1.00"), new BigDecimal("2.50"), new BigDecimal("1.50"));
		Proposal proposal2 = new Proposal(null, "Rejeitada", "user2", System.currentTimeMillis(), "Concorrente C", "111.111.111-11", "87654321", "brown@gmail.com", "Atividade 4", new BigDecimal("3.00"), new BigDecimal("8.00"), new BigDecimal("3.00"), new BigDecimal("8.50"));
		
		
		userRepository.saveAll(Arrays.asList(user1, user2, admin));
		rivalRepository.saveAll(Arrays.asList(rival1, rival2, rival3));
		activityRepository.saveAll(Arrays.asList(activity1, activity2, activity3, activity4));
		proposalRepository.saveAll(Arrays.asList(proposal1, proposal2));
	}
}
