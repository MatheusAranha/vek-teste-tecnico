package com.vektestbackend.server.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vektestbackend.server.domain.Proposal;

public class ProposalDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String status;
	private String user;
	private long timeStamp;
	private String rivalName;
	private String cnpjOrCpf;
	private String phoneNumber;
	private String email;
	private String activityName;
	private BigDecimal rivalDebitTax;
	private BigDecimal proposalDebitTax;
	private BigDecimal ribalCreditTax;
	private BigDecimal proposalCreditTax;
	
	public ProposalDTO() {
		
	}
	
	public ProposalDTO(Proposal obj) {
		id = obj.getId();
		status = obj.getStatus();
		user = obj.getUser();
		timeStamp = obj.getTimeStamp();
		rivalName = obj.getRivalName();
		cnpjOrCpf = obj.getCnpjOrCpf();
		phoneNumber = obj.getPhoneNumber();
		email = obj.getEmail();
		activityName = obj.getActivityName();
		rivalDebitTax = obj.getRivalDebitTax();
		proposalDebitTax = obj.getProposalDebitTax();
		ribalCreditTax = obj.getRibalCreditTax();
		proposalCreditTax = obj.getProposalCreditTax();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getRivalName() {
		return rivalName;
	}

	public void setRivalName(String rivalName) {
		this.rivalName = rivalName;
	}

	public String getCnpjOrCpf() {
		return cnpjOrCpf;
	}

	public void setCnpjOrCpf(String cnpjOrCpf) {
		this.cnpjOrCpf = cnpjOrCpf;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public BigDecimal getRivalDebitTax() {
		return rivalDebitTax;
	}

	public void setRivalDebitTax(BigDecimal rivalDebitTax) {
		this.rivalDebitTax = rivalDebitTax;
	}

	public BigDecimal getProposalDebitTax() {
		return proposalDebitTax;
	}

	public void setProposalDebitTax(BigDecimal proposalDebitTax) {
		this.proposalDebitTax = proposalDebitTax;
	}

	public BigDecimal getRibalCreditTax() {
		return ribalCreditTax;
	}

	public void setRibalCreditTax(BigDecimal ribalCreditTax) {
		this.ribalCreditTax = ribalCreditTax;
	}

	public BigDecimal getProposalCreditTax() {
		return proposalCreditTax;
	}

	public void setProposalCreditTax(BigDecimal proposalCreditTax) {
		this.proposalCreditTax = proposalCreditTax;
	}
	
	
}
