package com.vektestbackend.server.dto;

import java.io.Serializable;

public class SearchProposalDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String user;
	
	public SearchProposalDTO() {
		
	}

	public SearchProposalDTO(String user) {
		super();
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	
}
