package com.vektestbackend.server.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vektestbackend.server.domain.Rival;

public class RivalDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private BigDecimal debitTax;
	private BigDecimal creditTax;
	
	public RivalDTO() {
		
	}
	
	public RivalDTO(Rival obj) {
		id = obj.getId();
		name = obj.getName();
		debitTax = obj.getDebitTax();
		creditTax = obj.getCreditTax();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDebitTax() {
		return debitTax;
	}

	public void setDebitTax(BigDecimal debitTax) {
		this.debitTax = debitTax;
	}

	public BigDecimal getCreditTax() {
		return creditTax;
	}

	public void setCreditTax(BigDecimal creditTax) {
		this.creditTax = creditTax;
	}
	
	
}
