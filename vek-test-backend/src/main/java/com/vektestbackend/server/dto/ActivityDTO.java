package com.vektestbackend.server.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.vektestbackend.server.domain.Activity;

public class ActivityDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private BigDecimal debitMin;
	private BigDecimal creditMin;
	
	public ActivityDTO() {
		
	}
	
	public ActivityDTO(Activity obj) {
		id = obj.getId();
		name = obj.getName();
		debitMin = obj.getDebitMin();
		creditMin = obj.getCreditMin();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getDebitMin() {
		return debitMin;
	}

	public void setDebitMin(BigDecimal debitMin) {
		this.debitMin = debitMin;
	}

	public BigDecimal getCreditMin() {
		return creditMin;
	}

	public void setCreditMin(BigDecimal creditMin) {
		this.creditMin = creditMin;
	}
	
	
}
