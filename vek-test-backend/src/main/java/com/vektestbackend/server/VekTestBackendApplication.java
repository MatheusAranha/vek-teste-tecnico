package com.vektestbackend.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VekTestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(VekTestBackendApplication.class, args);
	}

}
