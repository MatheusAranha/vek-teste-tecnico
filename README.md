# VEK-Teste tecnico

Para rodar a aplicação, é necessário ter um banco de dados MongoDB rodando na porta 27017 (padrão do comando mongod)

A aplicação possui um login com 2 categorias de usuários, sendo elas:
 - Operadores: capazes de visualizar as propostas antigas e realizar novas propostas
 - Admin: capazes de visualizar as propostas antigas

As contas para realizar o login são:

 - Usuário: user1
   Senha: 123a

 - Usuário: user2
   Senha: 123b

 - Usuário: admin
   Senha: abc1

Ao registrar uma nova proposta, quando o usuário selecionar um concorrente/ramo de atividade, as informações de taxa do concorrente/taxas mínimas são automáticamente
adicionadas à seus respectivos campos, sem impedir que o usuário altere elas posteriormente